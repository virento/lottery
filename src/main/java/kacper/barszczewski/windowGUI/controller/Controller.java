package kacper.barszczewski.windowGUI.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import kacper.barszczewski.ApplicationInterface;
import kacper.barszczewski.GUI;
import kacper.barszczewski.entity.Lottery;

import java.io.IOException;

public class Controller {

    private ApplicationInterface appInterface;

    private Object controller;

    @FXML
    private AnchorPane rootPane;

    private GUI gui;

    @FXML
    private void initialize() throws IOException {
        showMenuPane();
    }

    public void showMenuPane() throws IOException {
        gui = GUI.MENU;
        MenuController controller = new MenuController(this);
        controller.setApplicationInterface(appInterface);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("Menu.fxml"));
        fxmlLoader.setController(controller);
        Parent parent = fxmlLoader.load();
        rootPane.getChildren().setAll(parent);
    }

    public void showNewLotteryPane() throws IOException {
        gui = GUI.CREATE;
        NewLotteryController controller = new NewLotteryController(this);
        this.controller = controller;
        controller.setApplicationInterface(appInterface);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("NewLottery.fxml"));
        fxmlLoader.setController(controller);
        Parent parent = fxmlLoader.load();
        rootPane.getChildren().setAll(parent);
    }

    public void showLotteryPane(Lottery lottery) throws IOException {
        gui = GUI.LOTTERY;
        LotteryController controller = new LotteryController(this, lottery);
        this.controller = controller;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("Lottery.fxml"));
        fxmlLoader.setController(controller);
        Parent parent = fxmlLoader.load();
        rootPane.getChildren().setAll(parent);
    }

    public void setApplicationInterface(ApplicationInterface windowApplication) {
        this.appInterface = windowApplication;
    }

    @FXML
    private void openConsole(ActionEvent event) {
        if (controller != null && controller.getClass().equals(NewLotteryController.class)) {
            ((NewLotteryController)controller).saveLotteryFields();
        }
        appInterface.switchToConsoleGui(gui);
        getStage().close();
    }

    public Stage getStage() {
        return (Stage) rootPane.getScene().getWindow();
    }

    public ApplicationInterface getApplicationInterface() {
        return appInterface;
    }
}
