package kacper.barszczewski.windowGUI.controller;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.FileChooser;
import kacper.barszczewski.ApplicationInterface;
import kacper.barszczewski.Utils;
import kacper.barszczewski.entity.*;
import kacper.barszczewski.windowGUI.WindowUtils;
import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;


public class NewLotteryController {

    @FXML
    private TextField titleField;

    @FXML
    private TextArea descriptionField;

    @FXML
    private TextField newParticipantField;

    @FXML
    private TextField newPrizeField;

    @FXML
    private TextField prizeQuantityField;

    @FXML
    private ListView<Participant> participantsListView;

    @FXML
    private ListView<Prize> prizesListView;

    @FXML
    private ChoiceBox<LotteryAlgorithm> typeField;

    private ApplicationInterface appInterface;

    private Controller controller;

    private Lottery lottery = new Lottery();

    private ObservableList<Prize> prizesObservableList;
    private ObservableList<Participant> participantsObservableList;

    public NewLotteryController(Controller controller) {
        this.controller = controller;
        if (controller.getApplicationInterface().getCreateLottery() != null) {
            this.lottery = controller.getApplicationInterface().getCreateLottery();
        } else {
            controller.getApplicationInterface().setCreateLottery(lottery);
        }
        prizesObservableList = FXCollections.observableList(lottery.getPrizes());
        participantsObservableList = FXCollections.observableList(lottery.getParticipants());
    }

    @FXML
    private void initialize() {
        prizeQuantityField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                prizeQuantityField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });

        prizesListView.setItems(prizesObservableList);
        prizesListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        participantsListView.setItems(participantsObservableList);
        participantsListView.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        titleField.setText(lottery.getLotteryName());
        descriptionField.setText(lottery.getLotteryDescription());
        newParticipantField.setOnAction(this::onAddParticipant);
        newPrizeField.setOnAction(this::onAddPrize);
        typeField.setItems(FXCollections.observableList(Arrays.asList(new OneWinnerAlgorithm(), new ManyWinnersAlgorithm())));
        for (LotteryAlgorithm item : typeField.getItems()) {
            if (item.getClass().equals(lottery.getAlgorithm().getClass())) {
                typeField.setValue(item);
                break;
            }
        }
    }

    public void setApplicationInterface(ApplicationInterface appInterface) {
        this.appInterface = appInterface;
    }

    @FXML
    public void onParticipantRemove(ActionEvent actionEvent) {
        List<Integer> selectedItems = new ArrayList<>(participantsListView.getSelectionModel().getSelectedIndices());
        selectedItems.sort((o1, o2) -> -1 * o1.compareTo(o2));
        for (int selectedItem : selectedItems) {
            participantsObservableList.remove(selectedItem);
        }
    }

    @FXML
    public void onPrizeRemove(ActionEvent actionEvent) {
        List<Integer> selectedItems = new ArrayList<>(prizesListView.getSelectionModel().getSelectedIndices());
        selectedItems.sort((o1, o2) -> -1 * o1.compareTo(o2));
        for (int selectedItem : selectedItems) {
            prizesObservableList.remove(selectedItem);
        }
    }

    @FXML
    public void onCreateLotteryClick(ActionEvent actionEvent) {
        if (participantsObservableList.size() == 0) {
            WindowUtils.showInformationDialog("Can't create lottery", "Please add at least one participant to lottery");
        } else if (prizesObservableList.size() == 0) {
            WindowUtils.showInformationDialog("Can't create lottery", "Please add at least one prize to lottery");
        } else {
            saveLotteryFields();
            controller.getApplicationInterface().createLottery(lottery);
            try {
                controller.showLotteryPane(lottery);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @FXML
    public void onAddParticipant(ActionEvent actionEvent) {
        String participantName = newParticipantField.getText();
        if (StringUtils.isBlank(participantName)) {
            return;
        }
        Participant participant = new Participant(participantName);
        participantsObservableList.add(participant);
        newParticipantField.setText("");
    }

    @FXML
    public void onAddPrize(ActionEvent actionEvent) {
        String prizeName = newPrizeField.getText();
        if (StringUtils.isBlank(prizeName)) {
            return;
        }
        int quantity = 1;
        if (prizeQuantityField.getText().trim().length() > 0) {
            quantity = Integer.parseInt(prizeQuantityField.getText());
        }
        prizeQuantityField.setText("");
        Prize prize = new Prize(quantity, prizeName);
        prizesObservableList.add(prize);
        newPrizeField.setText("");
    }

    @FXML
    public void onLoadParticipantsFromFile(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open participant file");
        File file = fileChooser.showOpenDialog(controller.getStage());
        participantsObservableList.addAll(Utils.participantsFromFile(file));
    }

    @FXML
    public void onLoadPrizesFromFile(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open prizes file");
        File file = fileChooser.showOpenDialog(controller.getStage());
        prizesObservableList.addAll(Utils.prizesFromFile(file));
    }

    @FXML
    public void onCancelClick(ActionEvent actionEvent) throws IOException {
        controller.showMenuPane();
        appInterface.setCreateLottery(null);
    }

    public void saveLotteryFields() {
        lottery.setAlgorithm(new ManyWinnersAlgorithm());
        lottery.setLotteryName(titleField.getText());
        lottery.setLotteryDescription(descriptionField.getText());
        lottery.setParticipants(new ArrayList<>(participantsObservableList));
        lottery.setPrizes(new ArrayList<>(prizesObservableList));
        lottery.setAlgorithm(typeField.getValue());
    }
}
