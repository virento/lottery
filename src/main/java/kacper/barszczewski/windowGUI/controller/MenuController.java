package kacper.barszczewski.windowGUI.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.stage.FileChooser;
import kacper.barszczewski.ApplicationInterface;
import kacper.barszczewski.Utils;
import kacper.barszczewski.entity.Lottery;
import kacper.barszczewski.windowGUI.WindowUtils;

import java.io.File;
import java.io.IOException;

public class MenuController {

    @FXML
    private ListView<Lottery> lotteriesList;

    @FXML
    private Button showDetailsClick;

    @FXML
    private Button exportButton;

    private ApplicationInterface appInterface;

    private Controller controller;


    public MenuController(Controller controller) {
        this.controller = controller;
    }

    @FXML
    private void initialize() {
        ObservableList<Lottery> lotteries = FXCollections.observableArrayList(appInterface.getLotteries());
        lotteriesList.setItems(lotteries);
        lotteriesList.getSelectionModel().selectedItemProperty().addListener((observableValue, lotteryMultipleSelectionModel, t1) -> {

        });
        lotteriesList.getSelectionModel().selectedItemProperty().addListener((observableValue, lottery, t1) -> {
            if (t1 != null) {
                exportButton.setDisable(false);
                showDetailsClick.setDisable(false);
            } else {
                exportButton.setDisable(true);
                showDetailsClick.setDisable(true);
            }
        });
    }

    @FXML
    private void onCreateLotteryClick(ActionEvent actionEvent) throws IOException {
        controller.showNewLotteryPane();
    }

    @FXML
    private void onShowDetailsClick(ActionEvent actionEvent) throws IOException {
        controller.showLotteryPane(lotteriesList.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void onExportClick(ActionEvent actionEvent) {
        Lottery lottery = lotteriesList.getSelectionModel().getSelectedItem();
        FileChooser chooser = new FileChooser();
        chooser.setTitle("Save lottery to");
        chooser.setInitialFileName(lottery.getLotteryName() + ".txt");
        File file = chooser.showSaveDialog(controller.getStage());
        if (file != null) {
            Utils.exportToFile(lottery, file);
            WindowUtils.showInformationDialog("Export status", "Lottery has been saved to file " + file.getName());
        }
    }

    public void setApplicationInterface(ApplicationInterface appInterface) {
        this.appInterface = appInterface;
    }
}
