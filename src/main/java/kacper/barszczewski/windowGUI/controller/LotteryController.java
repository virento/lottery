package kacper.barszczewski.windowGUI.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import kacper.barszczewski.entity.Lottery;
import kacper.barszczewski.entity.Prize;

import java.io.IOException;
import java.util.Set;

public class LotteryController {

    @FXML
    private Label nameField;
    @FXML
    private Label descriptionField;
    @FXML
    private GridPane winnersGridPane;

    private Controller controller;
    private Lottery lottery;
    public LotteryController(Controller controller, Lottery lottery) {
        this.controller = controller;
        this.lottery = lottery;
    }

    @FXML
    private void initialize() {
        nameField.setText(lottery.getLotteryName());
        descriptionField.setText(lottery.getLotteryDescription());
        Set<Prize> prizes = lottery.getWinners().keySet();
        int i = 0;
        for (Prize prize : prizes) {
            winnersGridPane.addRow(i++, new Label(prize.getName()), new Label(lottery.getWinners().get(prize).getName()));
        }
    }

    @FXML
    private void onBackClick(ActionEvent event) {
        try {
            controller.showMenuPane();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
