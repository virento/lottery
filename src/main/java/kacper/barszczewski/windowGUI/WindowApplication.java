package kacper.barszczewski.windowGUI;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import kacper.barszczewski.ApplicationInterface;
import kacper.barszczewski.GUI;
import kacper.barszczewski.windowGUI.controller.Controller;

import java.io.IOException;
import java.util.List;

public class WindowApplication extends Application {

    private ApplicationInterface applicationInterface;

    public static WindowApplication windowApplication;

    public WindowApplication() {
        WindowApplication.windowApplication = this;
        this.applicationInterface = kacper.barszczewski.Application.getInstance();
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        start(primaryStage, GUI.fromValue(getParameters().getRaw().get(0)));
//        Controller controller = new Controller();
//        controller.setApplicationInterface(applicationInterface);
//        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("Content.fxml"));
//        fxmlLoader.setController(controller);
//        Parent parent = fxmlLoader.load();
//
//        primaryStage.setTitle("Heloo World");
//        primaryStage.setScene(new Scene(parent, 800, 600));
//        primaryStage.show();
//
//        showProperController(controller);
    }

    private void showProperController(Controller controller, GUI gui) throws IOException {
        switch (gui) {
            case LOTTERY:
                controller.showLotteryPane(applicationInterface.getLotteries().get(0));
                return;
            case CREATE:
                controller.showNewLotteryPane();
                return;
        }
    }

    private void start(Stage stage, GUI gui) throws IOException {
        Controller controller = new Controller();
        controller.setApplicationInterface(applicationInterface);
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("Content.fxml"));
        fxmlLoader.setController(controller);
        Parent parent = fxmlLoader.load();
        stage.setOnCloseRequest(windowEvent -> System.exit(0));
        stage.setTitle("Heloo World");
        stage.setScene(new Scene(parent, 800, 600));
        stage.show();

        showProperController(controller, gui);
    }

    public static void lunch(GUI gui) {
        if (WindowApplication.windowApplication == null) {
            Platform.setImplicitExit(false);
            Application.launch(WindowApplication.class, gui.getValue());
            return;
        }
        Platform.runLater(() -> {
            try {
                WindowApplication.windowApplication.start(new Stage(), gui);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }
}
