package kacper.barszczewski.windowGUI;

import javafx.scene.control.Alert;

public class WindowUtils {

    public static void showInformationDialog(String title, String contentText) {
        Alert dialog = new Alert(Alert.AlertType.INFORMATION);
        dialog.setTitle(title);
        dialog.setContentText(contentText);
        dialog.show();
    }

}
