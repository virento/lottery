package kacper.barszczewski;

public enum GUI {
    MENU("M"), CREATE("C"), LOTTERY("L");

    private String value;

    GUI(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static GUI fromValue(String value) {
        for (GUI gui : GUI.values()) {
            if (gui.getValue().equals(value)) {
                return gui;
            }
        }
        return null;
    }
}
