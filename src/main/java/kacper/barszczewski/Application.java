package kacper.barszczewski;

import javafx.application.Platform;
import kacper.barszczewski.consoleGUI.ConsoleApplication;
import kacper.barszczewski.entity.Lottery;
import kacper.barszczewski.windowGUI.WindowApplication;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Application implements ApplicationInterface, Serializable {

    private ArrayList<Lottery> lotteries;

    private Lottery createLottery;

    private static Application application = new Application();

    public static Application getInstance() {
        return application;
    }

    private Application() {
        lotteries = new ArrayList<>();
    }

    private void showApplication() {
        switchToWindowGui(GUI.MENU);
    }

    @Override
    public void createLottery(Lottery lottery) {
        createLottery = null;
        lotteries.add(lottery);
        if (Utils.isEmpty(lottery.getLotteryName())) {
            lottery.setLotteryName("Unnamed lottery (" + lotteries.size() + ")");
        }
        if (Utils.isEmpty(lottery.getLotteryDescription())) {
            lottery.setLotteryDescription("No description");
        }
        lottery.roll();
    }

    @Override
    public Lottery getLastLottery() {
        if (lotteries.size() == 0) {
            return null;
        }
        return lotteries.get(lotteries.size() - 1);
    }

    @Override
    public int getLotterySize() {
        return lotteries.size();
    }

    @Override
    public void closeApplication() {
        Platform.exit();
        System.exit(0);
    }

    @Override
    public List<Lottery> getLotteries() {
        return new ArrayList<>(lotteries);
    }

    @Override
    public void switchToConsoleGui(GUI gui) {
        try {
            new ConsoleApplication(this, gui);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void switchToWindowGui(GUI gui) {
//        javafx.application.Application.launch(WindowApplication.class, gui.getValue());
        WindowApplication.lunch(gui);
//        try {
//            Thread.currentThread().join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }

    @Override
    public Lottery getCreateLottery() {
        return createLottery;
    }

    @Override
    public void setCreateLottery(Lottery lottery) {
        this.createLottery = lottery;
    }

    public static void main(String[] args) {
        getInstance().showApplication();
    }
}
