package kacper.barszczewski.consoleGUI.interfaces;

import kacper.barszczewski.consoleGUI.ConsoleApplication;

public interface MenuItemClickListener {
    public void onItemSelected(ConsoleApplication.MenuOption menuOption);
}
