package kacper.barszczewski.consoleGUI.interfaces;

public interface WindowCloseListener {
    public void onWindowClose(Object obj);
}
