package kacper.barszczewski.consoleGUI.interfaces;

import kacper.barszczewski.consoleGUI.layers.Layer;

public interface Controller {

    public void switchLayers(Layer newDrawable);


    public void showLayer(Layer oldDrawable, Layer newDrawable);

}
