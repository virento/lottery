package kacper.barszczewski.consoleGUI.interfaces;

public interface ButtonClickListener {
    public Object getResult();
}
