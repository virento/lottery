package kacper.barszczewski.consoleGUI.interfaces;

import com.googlecode.lanterna.input.KeyStroke;

import java.io.IOException;

public interface KeyListener {
    public boolean onKeyPressed(KeyStroke key) throws IOException;
}
