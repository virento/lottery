package kacper.barszczewski.consoleGUI.interfaces;

import kacper.barszczewski.entity.Lottery;

public interface LotteryCreatedListener {
    public void onLotteryCreated(Lottery lottery);
}
