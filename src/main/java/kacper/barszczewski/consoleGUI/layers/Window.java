package kacper.barszczewski.consoleGUI.layers;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextCharacter;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.BasicTextImage;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.graphics.TextImage;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.terminal.Terminal;
import kacper.barszczewski.GUI;
import kacper.barszczewski.consoleGUI.fields.InputField;
import kacper.barszczewski.consoleGUI.fields.ButtonField;
import kacper.barszczewski.consoleGUI.fields.Field;
import kacper.barszczewski.consoleGUI.interfaces.ButtonClickListener;
import kacper.barszczewski.consoleGUI.interfaces.WindowCloseListener;

import java.io.IOException;

public class Window extends Layer {

    private TerminalPosition tp;
    private TerminalSize size;

    private static final int MARGIN_LEFT_RIGHT = 10;
    private static final int MARGIN_TOP_BOTTOM = 3;

    public ButtonClickListener okButtonAction;
    public WindowCloseListener windowCloseListener;

    private ButtonField okButton;
    private ButtonField cancelButton;

    public Window(WindowCloseListener windowCloseListener) {
        this.windowCloseListener = windowCloseListener;
        createOkCancelButtons();
        addField(okButton);
        addField(cancelButton);
    }

    public void addFieldToWindow(Field... nFields) {
        for (Field field : nFields) {
            field.setColors(getBackgroundColor(), TextColor.ANSI.DEFAULT);
            addField(getFieldsSize() - 2, field);
        }
    }

    public void setOkButtonText(String text) {
        setButtonText(okButton, text);
    }

    public void setCancelButtonText(String text) {
        setButtonText(cancelButton, text);
    }

    private void setButtonText(ButtonField button, String text) {
        if (text == null) {
//            removeField(button);
            button.setCursorEnable(false);
            button.setLabel("");
        } else {
            button.setLabel("[" + text + "]");
        }
    }

    public void closeWindow(Object obj) {
        if(windowCloseListener != null) {
            windowCloseListener.onWindowClose(obj);
        }
    }

    @Override
    public void onLayoutAttached(Terminal terminal) throws IOException {
        super.onLayoutAttached(terminal);
        onResized(terminal, terminal.getTerminalSize());
    }

    @Override
    public void draw(Terminal terminal) throws IOException {
//        System.out.println("Draw: Window");
        TextGraphics tg = terminal.newTextGraphics();
        tg.setBackgroundColor(getBackgroundColor());
        TextImage image = new BasicTextImage(size);
        image.setAll(new TextCharacter(' ', null, getBackgroundColor()));
        tg.drawImage(tp, image);

        Field cursorField;
        for (int i = 0; i < getFieldsSize(); i++) {
            drawField(getField(i), tg, getStartY() + i);
        }
        cursorField = getField(activeRow);

        refreshCursorPosition(cursorField);
        terminal.flush();
    }

    @Override
    protected boolean onEnter(Field field) {
        if (!field.getFieldType().equals(Field.FieldType.ButtonField)) {
            return false;
        }
        ButtonField buttonField = (ButtonField) field;

        if (field == cancelButton) {
            closeWindow(null);
            return true;
        } else if (field == okButton) {
            if (okButtonAction != null) {
                closeWindow(okButtonAction.getResult());
            } else {
                closeWindow(null);
            }
            return true;
        }
        if (buttonField.haveOnClickEvent()) {
            buttonField.onButtonClick();
            return true;
        }
        return false;
    }

    @Override
    protected void drawField(Field field, TextGraphics tg, int line) {
        tg.setBackgroundColor(getBackgroundColor());
        field.draw(tg, new TerminalPosition(getStartX(), line));
    }

    @Override
    public void onLayerDetached(Terminal terminal) throws IOException {
        terminal.setBackgroundColor(TextColor.ANSI.BLACK);
        terminal.setForegroundColor(TextColor.ANSI.WHITE);
        terminal.clearScreen();
    }

    @Override
    protected int getStartX() {
        return tp.getColumn() + MARGIN_LEFT_RIGHT;
    }

    @Override
    protected int getStartY() {
        return tp.getRow() + MARGIN_TOP_BOTTOM;
    }

    public void setOnOkButtonClick(ButtonClickListener r) {
        this.okButtonAction = r;
    }

    @Override
    public boolean onKeyPressed(KeyStroke key) throws IOException {
        if (key.getKeyType() == KeyType.Character) {
            if ((key.getCharacter() == 'o' || key.getCharacter() == 'O') && key.isCtrlDown()) {
                onEnter(getField(getFieldsSize() - 2));
                return true;
            }
        } else if (key.getKeyType().equals(KeyType.ArrowRight) || key.getKeyType().equals(KeyType.ArrowLeft)) {
            int rel = key.getKeyType().equals(KeyType.ArrowRight) ? 1 : -1;
            if (getField(activeRow) == okButton || getField(activeRow) == cancelButton) {
                selectRelField(rel);
                return true;
            }
        }
        return super.onKeyPressed(key);
    }

    @Override
    public void onResized(Terminal terminal, TerminalSize newSize) {
        size = newSize;
        int width = MARGIN_LEFT_RIGHT * 2 + okButton.getLabel().length() + cancelButton.getLabel().length() + 3;
        int height = getFieldsSize() - 2 + 2 * MARGIN_TOP_BOTTOM;
        if (height % 2 != (getFieldsSize() - 2) % 2) {
            height++;
        }

        int wSize = 0;
        for (int i = 0; i < getFieldsSize(); i++) {
            Field field = getField(i);
            int fieldWidth = field.getFieldWidth() + 2 * MARGIN_LEFT_RIGHT;
            if (wSize < fieldWidth) {
                wSize = fieldWidth;
            }
        }
        if (wSize > width) {
            width = wSize;
        }
        size = size.withColumns(width).withRows(height);
        tp = new TerminalPosition(0, 0);
        tp = tp.withColumn((newSize.getColumns() - size.getColumns()) / 2).withRow((newSize.getRows() - size.getRows()) / 2);
    }

    @Override
    public GUI getGui() {
        return GUI.MENU;
    }

    private void createOkCancelButtons() {
        okButton = new ButtonField("[OK]") {
            @Override
            public void draw(TextGraphics tg, TerminalPosition pos) {
                super.draw(tg, new TerminalPosition(getStartX() + 3, getStartY() + getFieldsSize() - 1));
            }

            @Override
            public void refreshCursorPosition(Terminal terminal, TerminalPosition pos) throws IOException {
                super.refreshCursorPosition(terminal, new TerminalPosition(getStartX() + 3, getStartY() + getFieldsSize() - 1));
            }
        };
        cancelButton = new ButtonField("[CANCEL]") {
            @Override
            public void draw(TextGraphics tg, TerminalPosition pos) {
                super.draw(tg, new TerminalPosition(getStartX() + okButton.getLabel().length() + 5, getStartY() + getFieldsSize() - 1));
            }

            @Override
            public void refreshCursorPosition(Terminal terminal, TerminalPosition pos) throws IOException {
                super.refreshCursorPosition(terminal, new TerminalPosition(getStartX() + okButton.getLabel().length() + 5, getStartY() + getFieldsSize() - 1));
            }
        };
    }


    public static Window AlertWindow(WindowCloseListener listener, String title) {
        Window window = new Window(listener);
        InputField field = new InputField(title);
        field.setCursorEnable(false);
        window.addFieldToWindow(field);
        window.setCancelButtonText(null);
        return window;
    }

    private TextColor.ANSI getBackgroundColor() {
        return TextColor.ANSI.BLUE;
    }
}
