package kacper.barszczewski.consoleGUI.layers;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.FileDialog;
import com.googlecode.lanterna.gui2.dialogs.FileDialogBuilder;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame;
import kacper.barszczewski.*;
import kacper.barszczewski.entity.*;
import kacper.barszczewski.consoleGUI.fields.ButtonField;
import kacper.barszczewski.consoleGUI.fields.Field;
import kacper.barszczewski.consoleGUI.fields.InputField;
import kacper.barszczewski.consoleGUI.fields.ListField;
import kacper.barszczewski.consoleGUI.interfaces.LotteryCreatedListener;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class NewLottery extends Layer {

    private static final int LOTTERY_NAME = 0;
    private static final int LOTTERY_DESC = 1;
    private static final int PRIZES_LIST = 4;
    private static final int PARTICIPANTS_LIST = 7;

    private LotteryAlgorithm lotteryAlgorithm = new OneWinnerAlgorithm();

    private LotteryCreatedListener listener;

    public static NewLottery newInstance(LotteryCreatedListener listener, Lottery createLottery) {
        NewLottery lottery = new NewLottery(createLottery);
        lottery.setListener(listener);
        return lottery;
    }

    private NewLottery(Lottery createLottery) {
        addField(LOTTERY_NAME, new InputField("Lottery name: ", 20));
        addField(LOTTERY_DESC, new InputField("Description: ", 50));
        addField(2, new Field("Prizes: "));
        addField(3, new ButtonField("<Add prize>"));
        addField(PRIZES_LIST, new ListField<Prize>(""));
        addField(5, new Field("Participants: "));
        addField(6, new ButtonField("<Add participant>"));
        addField(PARTICIPANTS_LIST, new ListField<Participant>(""));
        ButtonField typeField = new ButtonField(lotteryAlgorithm.toString());
        typeField.setTitle("Type: ");
        typeField.setOnClick(() -> {
            Window window = new Window(null);
            InputField labelField = new InputField("Choose one:");
            labelField.setCursorEnable(false);
            ButtonField but1 = new ButtonField("One prize for one person");
            but1.setOnClick(() -> {
                lotteryAlgorithm = new OneWinnerAlgorithm();
                typeField.setLabel(lotteryAlgorithm.toString());
                closeWindow(null);
            });
            ButtonField but2 = new ButtonField("Many prize for one person");
            but2.setOnClick(() -> {
                lotteryAlgorithm = new ManyWinnersAlgorithm();
                typeField.setLabel(lotteryAlgorithm.toString());
                closeWindow(null);
            });
            window.addFieldToWindow(labelField, but1, but2);
            window.setCancelButtonText(null);
            window.setOkButtonText(null);
            showWindow(window);
        });
        addField(8, typeField);
        if (createLottery != null) {
            ((InputField) getField(LOTTERY_NAME)).setValue(createLottery.getLotteryName());
            ((InputField) getField(LOTTERY_DESC)).setValue(createLottery.getLotteryDescription());
            ((ListField<Prize>) getField(PRIZES_LIST)).setList(createLottery.getPrizes());
            ((ListField<Participant>) getField(PARTICIPANTS_LIST)).setList(createLottery.getParticipants());
            lotteryAlgorithm = createLottery.getAlgorithm();
            typeField.setLabel(lotteryAlgorithm.toString());
        }
    }

    public LotteryCreatedListener getListener() {
        return listener;
    }

    public void setListener(LotteryCreatedListener listener) {
        this.listener = listener;
    }

    @Override
    public void onLayoutAttached(Terminal terminal) throws IOException {
        super.onLayoutAttached(terminal);
    }

    @Override
    public void draw(Terminal terminal) throws IOException {
//        System.out.println("Draw: NewLottery");
        TextGraphics tg = terminal.newTextGraphics();
        tg.setBackgroundColor(TextColor.ANSI.DEFAULT);
        tg.setForegroundColor(TextColor.ANSI.DEFAULT);
        for (int i = 0; i < getFieldsSize(); i++) {
            getField(i).draw(tg, new TerminalPosition(getStartX(), getStartY() + i));
        }
        Field cursorField = getField(activeRow);
        TerminalSize size = terminal.getTerminalSize();
        tg.putString(0, size.getRows() - 4, "CTRL + G - Switch to Window GUI");
        tg.putString(0, size.getRows() - 3, "CTRL + P - load prizes from file");
        tg.putString(0, size.getRows() - 2, "CTRL + L - load participants from file");
        tg.putString(0, size.getRows() - 1, "ESC - return to menu, CTRL + N - create lottery");

        refreshCursorPosition(cursorField);
        terminal.flush();
        super.draw(terminal);
    }

    @Override
    public void onWindowResult(Object obj) throws IOException {
        if (obj instanceof Prize) {
            ListField<Prize> list = getPrizesField();
            list.addItem((Prize) obj);
            draw(terminal);
        } else if (obj instanceof Participant) {
            ListField<Participant> list = getParticipantsField();
            list.addItem((Participant) obj);
            draw(terminal);
        }
        super.onWindowResult(obj);
    }

    @Override
    public GUI getGui() {
        return GUI.CREATE;
    }

    @Override
    public void onSwitching(ApplicationInterface applicationInterface) {
        Lottery lottery = applicationInterface.getCreateLottery();
        if (lottery == null) {
            lottery = new Lottery();
        }
        fillLottery(lottery);
        applicationInterface.setCreateLottery(lottery);
    }

    @Override
    public boolean onKeyPressed(KeyStroke key) throws IOException {
        if (key.getKeyType() == KeyType.Character && key.isCtrlDown()) {
            if (Utils.isEqual(key.getCharacter(), 'l')) {
                showParticipantFileDialog();
                return true;
            } else if (Utils.isEqual(key.getCharacter(), 'n')) {
                Lottery lottery = getLottery();
                if (lottery == null) {
                    return true;
                }
                onLotteryCreated(lottery);
            } else if (Utils.isEqual(key.getCharacter(), 'p')) {
                showLotteryFileDialog();
                return true;
            }
        }
        return super.onKeyPressed(key);
    }

    private void onLotteryCreated(Lottery lottery) {
        if (getListener() != null) {
            listener.onLotteryCreated(lottery);
        }
    }

    private Lottery getLottery() {
        ArrayList<Participant> participents = getParticipantsField().getList();
        ArrayList<Prize> prizes = getPrizesField().getList();
        if (prizes.size() == 0) {
            showWindow("Please add at least one prize");
            return null;
        }
        if (participents.size() == 0) {
            showWindow("Please add at least one participant");
            return null;
        }
        Lottery lottery = new Lottery();
        fillLottery(lottery);
        return lottery;
    }

    private void fillLottery(Lottery lottery) {
        lottery.setLotteryName(((InputField) getField(LOTTERY_NAME)).getValue());
        lottery.setLotteryDescription(((InputField) getField(LOTTERY_DESC)).getValue());
        lottery.setAlgorithm(lotteryAlgorithm);
        lottery.setParticipants(getParticipantsField().getList());
        lottery.setPrizes(getPrizesField().getList());
    }

    private void showWindow(String message) {
        Window window = new Window(this::closeWindow);
        InputField field = new InputField(message);
        field.setCursorEnable(false);
        window.addFieldToWindow(field);
        showWindow(window);
    }

    private void showLotteryFileDialog() throws IOException {
        File f = getFileFromDialog("Import prizes");
        getPrizesField().getList().addAll(Utils.prizesFromFile(f));
        terminal.setBackgroundColor(TextColor.ANSI.BLACK);
        terminal.setForegroundColor(TextColor.ANSI.WHITE);
        terminal.clearScreen();
        draw(terminal);
    }

    private void showParticipantFileDialog() throws IOException {
        File f = getFileFromDialog("Import participants");
        getParticipantsField().getList().addAll(Utils.participantsFromFile(f));
        terminal.setBackgroundColor(TextColor.ANSI.BLACK);
        terminal.setForegroundColor(TextColor.ANSI.WHITE);
        terminal.clearScreen();
        draw(terminal);
    }

    private File getFileFromDialog(String title) throws IOException {
        TerminalScreen screen = new TerminalScreen(terminal);
        screen.startScreen();
        WindowBasedTextGUI gui = new MultiWindowTextGUI(screen);
        FileDialog builder = new FileDialogBuilder()
                .setTitle(title)
                .build();
        builder.setCloseWindowWithEscape(true);
        File f = builder.showDialog(gui);
        if (!(terminal instanceof SwingTerminalFrame)) {
            screen.stopScreen();
        }
        return f;
    }

    @Override
    protected boolean onEnter(Field field) {
        if (super.onEnter(field)) {
            return true;
        }
        Window window = new Window(this::closeWindow);
        if (field == getField(PRIZES_LIST - 1)) {
            InputField prodF = new InputField("Product: ", 20);
            InputField quanF = new InputField("Quantity: ", 5);
            window.addFieldToWindow(prodF, quanF);
            window.setOnOkButtonClick(() -> {
                int quantity = 1;
                try {
                    String value = quanF.getValue();
                    if (value.length() > 0) {
                        quantity = Integer.parseInt(quanF.getValue());
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    return null;
                }
                return new Prize(quantity, prodF.getValue());
            });
            showWindow(window);
        } else if (field == getField(PARTICIPANTS_LIST - 1)) {
            InputField nameF = new InputField("Name: ", 20);
            window.addFieldToWindow(nameF);
            window.setOnOkButtonClick(() -> new Participant(nameF.getValue()));
            showWindow(window);
        } else if (field.getFieldType().equals(Field.FieldType.ListField)) {
            ListField list = (ListField) field;
            Value value = list.getSelectedField();
            InputField label = new InputField("Remove " + value.getValue() + "?");
            label.setCursorEnable(false);
            window.addFieldToWindow(label);
            window.setOkButtonText("Yes");
            window.setCancelButtonText("No");
            window.setOnOkButtonClick(() -> {
                list.removeItem(value);
                return null;
            });
            showWindow(window);
        }
        return false;
    }

    private ListField<Prize> getPrizesField() {
        return (ListField<Prize>) getField(PRIZES_LIST);
    }

    private ListField<Participant> getParticipantsField() {
        return (ListField<Participant>) getField(PARTICIPANTS_LIST);
    }
}
