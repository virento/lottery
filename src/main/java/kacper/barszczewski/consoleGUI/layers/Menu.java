package kacper.barszczewski.consoleGUI.layers;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalResizeListener;
import kacper.barszczewski.GUI;
import kacper.barszczewski.consoleGUI.ConsoleApplication;
import kacper.barszczewski.consoleGUI.fields.ButtonField;
import kacper.barszczewski.consoleGUI.fields.Field;
import kacper.barszczewski.consoleGUI.interfaces.KeyListener;
import kacper.barszczewski.consoleGUI.interfaces.MenuItemClickListener;

import java.io.IOException;

public class Menu extends Layer implements TerminalResizeListener, KeyListener {

    private TerminalPosition tp;

    private static final int CENTER_OFFSET = 15;

    private MenuItemClickListener listener;

    public Menu() {
    }

    @Override
    public void onLayoutAttached(Terminal terminal) throws IOException {
        this.terminal = terminal;
        super.onLayoutAttached(terminal);
        onResized(terminal, terminal.getTerminalSize());
    }

    @Override
    public void draw(Terminal terminal) throws IOException {
        TextGraphics tg = terminal.newTextGraphics();
        for (int i = 0; i < getFieldsSize(); i++) {
            Field field = getField(i);
            field.draw(tg, getPosition(i));
        }

        TerminalSize size = terminal.getTerminalSize();
        tg.putString(0, size.getRows() - 1, "CTRL + G - Switch to Window GUI");

        refreshCursorPosition(getField(activeRow));
        terminal.flush();
        super.draw(terminal);
    }

    @Override
    protected int getStartY() {
        return tp.getRow();
    }

    @Override
    protected int getStartX() {
        return tp.getColumn();
    }

    private TerminalPosition getPosition(int i) {
        return tp.withRelativeRow(i);
    }

    @Override
    public void onLayerDetached(Terminal terminal) throws IOException {
        terminal.setCursorVisible(true);
        terminal.removeResizeListener(this);
    }

    @Override
    public void onResized(Terminal terminal, TerminalSize newSize) {
        try {
            TerminalSize size = terminal.getTerminalSize();
            int numOfMenu = getFieldsSize();
            tp = new TerminalPosition((size.getColumns() - CENTER_OFFSET) / 2, (size.getRows() - numOfMenu) / 2);
        } catch (IOException e) {
            e.printStackTrace();
            tp = new TerminalPosition(0, 0);
        }
        super.onResized(terminal, newSize);
    }

    @Override
    public GUI getGui() {
        return GUI.MENU;
    }

    private MenuItemClickListener getListener() {
        return listener;
    }

    public void setListener(MenuItemClickListener listener) {
        this.listener = listener;
    }

    public void addMenuItem(ConsoleApplication.MenuOption menuItem) {
        ButtonField field = new ButtonField(menuItem.getValue());
        field.setHighlightColors(TextColor.ANSI.CYAN, TextColor.ANSI.BLACK);
        field.setOnClick(() -> {
            if (getListener() != null) {
                getListener().onItemSelected(menuItem);
            }
        });
        addField(field);
    }
}
