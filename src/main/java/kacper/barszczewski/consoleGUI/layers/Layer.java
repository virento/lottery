package kacper.barszczewski.consoleGUI.layers;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.terminal.Terminal;
import kacper.barszczewski.ApplicationInterface;
import kacper.barszczewski.GUI;
import kacper.barszczewski.consoleGUI.fields.ButtonField;
import kacper.barszczewski.consoleGUI.fields.Field;
import kacper.barszczewski.consoleGUI.fields.InputField;
import kacper.barszczewski.consoleGUI.fields.ListField;
import kacper.barszczewski.consoleGUI.interfaces.KeyListener;

import java.io.IOException;
import java.util.ArrayList;

public abstract class Layer implements KeyListener {

    private ArrayList<Field> fields = new ArrayList<>();

    protected int activeRow = -1;

    protected Terminal terminal;

    private Layer upperLayer = null;

    protected void selectRelField(int off) throws IOException {
        changeSelection(activeRow, off, false);
    }

    protected void changeSelection(int selRow, int offVal, boolean loop) throws IOException {
        selRow += offVal;
        if (selRow == fields.size()) {
            if (loop) {
                selRow = 0;
            } else {
                return;
            }
        } else if (selRow < 0) {
            if (loop) {
                selRow = fields.size() - 1;
            } else {
                return;
            }
        }
        if (!fields.get(selRow).cursorEnable()) {
            changeSelection(selRow, offVal, loop);
            return;
        }

        fields.get(activeRow).onFocusLost();
        drawField(fields.get(activeRow), terminal.newTextGraphics(), getStartY() + activeRow);
        activeRow = selRow;
        fields.get(activeRow).onFocusGain();
        refreshCursorPosition(fields.get(selRow));
    }

    protected void drawField(Field field, TextGraphics tg, int line) throws IOException {
        field.draw(tg, new TerminalPosition(getStartX(), line));
    }

    public void onLayoutAttached(Terminal terminal) throws IOException {
        this.terminal = terminal;
        if (activeRow == -1) {
            for (int i = 0; i < fields.size(); i++) {
                if (fields.get(i).cursorEnable()) {
                    activeRow = i;
                    break;
                }
            }
        }
    }

    public void draw(Terminal terminal) throws IOException {
        if (upperLayer != null) {
            upperLayer.draw(terminal);
        }
    }

    public void onLayerDetached(Terminal terminal) throws IOException {

    }

    public void showWindow(Window window) {
        upperLayer = window;
        try {
            window.onLayoutAttached(terminal);
            window.draw(terminal);
        } catch (IOException e) {
            e.printStackTrace();
            upperLayer = null;
        }
    }

    public void closeWindow(Object obj) {
        try {
            onWindowResult(obj);
            upperLayer.onLayerDetached(terminal);
            upperLayer = null;
            draw(terminal);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void refreshCursorPosition(Field field) throws IOException {
        field.refreshCursorPosition(terminal, new TerminalPosition(getStartX(), getStartY() + activeRow));
        terminal.flush();
    }

    @Override
    public boolean onKeyPressed(KeyStroke key) throws IOException {
        if (upperLayer != null) {
            boolean handled = upperLayer.onKeyPressed(key);
            if (!handled && key.getKeyType().equals(KeyType.Escape)) {
                closeWindow(null);
                return true;
            }
            return handled;
        }
        if (key.isCtrlDown()) {
            return false;
        }
        switch (key.getKeyType()) {
            case ArrowDown:
                selectRelField(1);
                return true;
            case ArrowUp:
                selectRelField(-1);
                return true;
            case ArrowLeft:
                changeCursorPosition(fields.get(activeRow), -1, 0);
                return true;
            case ArrowRight:
                changeCursorPosition(fields.get(activeRow), 1, 0);
                return true;
            case Backspace:
                fields.get(activeRow).onBackspace(terminal, new TerminalPosition(getStartX(), getStartY() + activeRow));
                return true;
            case Character:
                fields.get(activeRow).onCharacter(key, terminal, new TerminalPosition(getStartX(), getStartY() + activeRow));
                return true;
            case Enter:
                onEnter(fields.get(activeRow));
                return true;
            case Tab:
                changeSelection(activeRow, 1, true);
                return true;
        }
        return false;
    }

    protected boolean onEnter(Field field) {
        if (field.getFieldType().equals(Field.FieldType.ButtonField)) {
            ButtonField button = (ButtonField) field;
            if (button.haveOnClickEvent()) {
                button.onButtonClick();
                return true;
            }
        }
        return false;
    }

    protected void changeCursorPosition(Field field, int xOff, int yOff) throws IOException {
        TerminalPosition pos = terminal.getCursorPosition();
        if (field.getFieldType().equals(Field.FieldType.InputField)) {
            InputField inField = (InputField) field;
            if ((xOff != getStartX() && inField.getValue().length() == 0) ||
                    (xOff > getStartX() && pos.getColumn() == field.getLabel().length() + inField.getValue().length()) ||
                    (xOff < getStartX() && pos.getColumn() == field.getLabel().length())) xOff = 0;
        } else if (field.getFieldType().equals(Field.FieldType.ListField)) {
            ListField lField = (ListField) field;
            if (xOff > 0) {
                lField.selectNext();
            } else if (xOff < 0) {
                lField.selectPrevious();
            }
            lField.draw(terminal.newTextGraphics(), new TerminalPosition(0, activeRow));
        }
        terminal.setCursorPosition(pos.withRelativeRow(yOff).withRelativeColumn(xOff));
        terminal.flush();
    }

    protected int getStartX() {
        return 0;
    }

    protected int getStartY() {
        return 0;
    }

    public void onWindowResult(Object obj) throws IOException {
    }

    public void onResized(Terminal terminal, TerminalSize newSize) {
        if (upperLayer != null) {
            upperLayer.onResized(terminal, newSize);
        }
    }

    public void addField(Field field) {
        fields.add(field);
    }

    public void addField(int id, Field field) {
        fields.add(id, field);
    }

    public int getFieldsSize() {
        return fields.size();
    }

    public Field getField(int i) {
        return fields.get(i);
    }

    public void removeField(Object obj) {
        fields.remove(obj);
    }

    public abstract GUI getGui();

    public void onSwitching(ApplicationInterface applicationInterface) {

    }
}
