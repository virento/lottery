package kacper.barszczewski.consoleGUI.layers;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.terminal.Terminal;
import kacper.barszczewski.GUI;
import kacper.barszczewski.Utils;
import kacper.barszczewski.entity.Lottery;
import kacper.barszczewski.entity.Participant;
import kacper.barszczewski.entity.Prize;
import kacper.barszczewski.consoleGUI.fields.ButtonField;
import kacper.barszczewski.consoleGUI.fields.Field;
import kacper.barszczewski.consoleGUI.fields.InputField;

import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public class LotteryView extends Layer {

    public static LotteryView newInstance(Lottery lottery) {
        return new LotteryView(lottery);
    }

    public LotteryView(Lottery lottery) {
        fillFields(lottery);
    }

    private void fillFields(Lottery lottery) {
        HashMap<Prize, Participant> winners = lottery.getWinners();
        Set<Prize> prizes = winners.keySet();
        InputField titleField = new InputField(lottery.getLotteryName());
        titleField.setColors(TextColor.ANSI.CYAN, TextColor.ANSI.BLACK);
        titleField.setCursorEnable(false);
        addField(titleField);
        if (!Utils.isEmpty(lottery.getLotteryDescription())) {
            InputField descField = new InputField(lottery.getLotteryDescription());
            descField.setCursorEnable(false);
            addField(descField);
        }
        InputField spaceField = new InputField("");
        spaceField.setCursorEnable(false);
        addField(spaceField);
        for (Prize prize : prizes) {
            addField(new ButtonField(winners.get(prize).getName() + ": " + prize.getName()));
        }
    }

    @Override
    public void onLayoutAttached(Terminal terminal) throws IOException {
        super.onLayoutAttached(terminal);
    }

    @Override
    public void draw(Terminal terminal) throws IOException {
        TextGraphics tg = terminal.newTextGraphics();
        tg.setBackgroundColor(TextColor.ANSI.DEFAULT);
        tg.setBackgroundColor(TextColor.ANSI.DEFAULT);
        int width = tg.getSize().getColumns();
        int startX;
        Field field;
        for (int i = 0; i < getFieldsSize(); i++) {
            field = getField(i);
            startX = (width - field.getFieldWidth()) / 2;
            field.draw(tg, new TerminalPosition(startX, getStartY() + i));
        }

        TerminalSize size = terminal.getTerminalSize();
        tg.putString(0, size.getRows() - 1, "CTRL + G - Switch to Window GUI");
        refreshCursorPosition(getField(activeRow));
        terminal.flush();
    }

    @Override
    protected void drawField(Field field, TextGraphics tg, int line) throws IOException {
        int width = terminal.getTerminalSize().getColumns();
        int startX = (width - field.getFieldWidth()) / 2;
        field.draw(tg, new TerminalPosition(startX, line));
    }

    @Override
    protected void refreshCursorPosition(Field field) throws IOException {
        int width = terminal.getTerminalSize().getColumns();
        int startX = (width - field.getFieldWidth()) / 2;
        field.refreshCursorPosition(terminal, new TerminalPosition(startX, getStartY() + activeRow));
        terminal.flush();
    }

    @Override
    public GUI getGui() {
        return GUI.LOTTERY;
    }
}
