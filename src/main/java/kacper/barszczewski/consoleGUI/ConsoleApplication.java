package kacper.barszczewski.consoleGUI;

import com.googlecode.lanterna.TerminalSize;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.gui2.MultiWindowTextGUI;
import com.googlecode.lanterna.gui2.WindowBasedTextGUI;
import com.googlecode.lanterna.gui2.dialogs.FileDialog;
import com.googlecode.lanterna.gui2.dialogs.FileDialogBuilder;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.input.KeyType;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import com.googlecode.lanterna.terminal.TerminalResizeListener;
import com.googlecode.lanterna.terminal.swing.SwingTerminalFrame;
import kacper.barszczewski.ApplicationInterface;
import kacper.barszczewski.Utils;
import kacper.barszczewski.consoleGUI.fields.InputField;
import kacper.barszczewski.entity.Lottery;
import kacper.barszczewski.consoleGUI.interfaces.Controller;
import kacper.barszczewski.consoleGUI.interfaces.KeyListener;
import kacper.barszczewski.consoleGUI.interfaces.MenuItemClickListener;
import kacper.barszczewski.consoleGUI.layers.*;
import kacper.barszczewski.GUI;

import java.io.File;
import java.io.IOException;
import java.util.Stack;

public class ConsoleApplication implements Controller, TerminalResizeListener, MenuItemClickListener {

    public enum MenuOption {
        CREATE_LOTTERY("Create lottery"),
        SHOW_LAST_LOTTERY("Last lottery"),
        EXPORT_LAST_LOTTERY("Export last lottery");

        String value;

        MenuOption(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }


    }

    private Terminal terminal = null;

    private Menu menu;

    private KeyListenerThread keyThread;

    private Layer activeLayer;

    private Stack<Layer> layerStack = new Stack<>();

    private ApplicationInterface applicationInterface;

    public ConsoleApplication(ApplicationInterface application, GUI gui) throws IOException {
        this.applicationInterface = application;
        init();
        showLayer(null, menu);
        terminal.addResizeListener(this);

        switch (gui) {
            case CREATE:
                switchLayers(NewLottery.newInstance(this::createAndShowLottery, application.getCreateLottery()));
                return;
            case LOTTERY:
                switchLayers(LotteryView.newInstance(applicationInterface.getLastLottery()));
                return;
        }
    }

    public ConsoleApplication(ApplicationInterface applicationInterface) throws IOException {
        this(applicationInterface, GUI.MENU);
    }

    private void init() throws IOException {
        DefaultTerminalFactory factory = new DefaultTerminalFactory();
        terminal = factory.createTerminal();

        menu = new Menu();
        menu.addMenuItem(MenuOption.CREATE_LOTTERY);
        menu.addMenuItem(MenuOption.SHOW_LAST_LOTTERY);
        menu.addMenuItem(MenuOption.EXPORT_LAST_LOTTERY);
        menu.setListener(this);
        activeLayer = menu;
        keyThread = new KeyListenerThread();
    }

    @Override
    public void onItemSelected(MenuOption menuOption) {
        switch (menuOption) {
            case CREATE_LOTTERY:
                switchLayers(NewLottery.newInstance(this::createAndShowLottery, null));
                break;
            case SHOW_LAST_LOTTERY:
                if (applicationInterface.getLotterySize() == 0) {
                    activeLayer.showWindow(Window.AlertWindow(obj -> activeLayer.closeWindow(null), "There is non lotteries created"));
                    return;
                }
                switchLayers(LotteryView.newInstance(applicationInterface.getLastLottery()));
                break;
            case EXPORT_LAST_LOTTERY:
                if (applicationInterface.getLotterySize() == 0) {
                    activeLayer.showWindow(Window.AlertWindow(obj -> activeLayer.closeWindow(null), "There is non lotteries created"));
                    return;
                }
                File file = showFileSaveWindow("Save lottery as...", new File("Lottery"));
                Utils.exportToFile(applicationInterface.getLastLottery(), file);
        }
    }

    @Override
    public void switchLayers(Layer newDrawable) {
        if (activeLayer != null) {
            layerStack.add(activeLayer);
        }
        showLayer(activeLayer, newDrawable);
    }

    @Override
    public void showLayer(Layer oldDrawable, Layer newDrawable) {
        if (newDrawable == null) return;
        try {
            Utils.changeLayer(terminal, oldDrawable, newDrawable);
            activeLayer = newDrawable;
            keyThread.setListener(activeLayer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void createAndShowLottery(Lottery lottery) {
        applicationInterface.createLottery(lottery);
        showLayer(activeLayer, new LotteryView(lottery));
    }

    public void closeApplication() {
        try {
            terminal.setCursorPosition(0, 0);
            terminal.setCursorVisible(true);
            terminal.setForegroundColor(TextColor.ANSI.DEFAULT);
            terminal.setBackgroundColor(TextColor.ANSI.DEFAULT);
            terminal.clearScreen();
            terminal.flush();
            terminal.close();
            keyThread.interrupt();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResized(Terminal terminal, TerminalSize newSize) {
        if (activeLayer == null) return;
        try {
            terminal.setBackgroundColor(TextColor.ANSI.BLACK);
            terminal.setForegroundColor(TextColor.ANSI.WHITE);
            terminal.clearScreen();
            activeLayer.onResized(terminal, newSize);
            activeLayer.draw(terminal);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private File showFileSaveWindow(String title, File selectedFile) {
        TerminalScreen screen;
        try {
            screen = new TerminalScreen(terminal);
            screen.startScreen();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
        WindowBasedTextGUI gui = new MultiWindowTextGUI(screen);
        FileDialog builder = new FileDialogBuilder()
                .setTitle(title)
                .setSelectedFile(selectedFile)
                .build();
        builder.setCloseWindowWithEscape(true);
        File file = builder.showDialog(gui);
        try {
            if (!(terminal instanceof SwingTerminalFrame)) {
                screen.stopScreen();
            }
            terminal.setBackgroundColor(TextColor.ANSI.BLACK);
            terminal.setForegroundColor(TextColor.ANSI.WHITE);
            terminal.clearScreen();
            activeLayer.draw(terminal);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

    private void showCloseConfirmWindow() {
        Window window = new Window(obj -> activeLayer.closeWindow(null));
        InputField title = new InputField("Do you wanna close application?");
        title.setCursorEnable(false);
        window.addFieldToWindow(title);
        window.setCancelButtonText("Cancel");
        window.setOkButtonText("Exit");
        window.setOnOkButtonClick(() -> {
            closeApplication();
            applicationInterface.closeApplication();
            return null;
        });
        activeLayer.showWindow(window);
    }

    private class KeyListenerThread extends Thread {


        private KeyListener listener;

        KeyListenerThread() {

        }

        void setListener(KeyListener listener) {
            this.listener = listener;
            if (!isAlive()) {
                start();
            }
        }

        @Override
        public void run() {
            while (!isInterrupted()) {
                try {
                    KeyStroke key = terminal.readInput();
                    if (key.getKeyType() == KeyType.EOF) {
                        interrupt();
                        continue;
                    }
                    boolean handled = false;
                    if (listener != null) {
                        handled = listener.onKeyPressed(key);
                    }
                    if (!handled) {
                        if (key.getKeyType() == KeyType.Escape) {
                            if (!layerStack.empty()) {
                                showLayer(activeLayer, layerStack.pop());
                            } else {
                                showCloseConfirmWindow();
                            }
                        } else if (key.isCtrlDown() && key.getKeyType() == KeyType.Character) {
                            if (Utils.isEqual(key.getCharacter(), 'g')) {
                                activeLayer.onSwitching(applicationInterface);
                                applicationInterface.switchToWindowGui(activeLayer.getGui());
                                closeApplication();
                            }
                        }
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

}
