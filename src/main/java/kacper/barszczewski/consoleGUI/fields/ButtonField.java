package kacper.barszczewski.consoleGUI.fields;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.terminal.Terminal;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;

@Getter
@Setter
public class ButtonField extends Field {

    private String title = null;

    private Runnable onClick = null;

    private TextColor highlightBackgroundColor = TextColor.ANSI.YELLOW;
    private TextColor highlightForegroundColor = TextColor.ANSI.DEFAULT;

    public ButtonField(String label) {
        super(label);
        setCursorEnable(true);
        setColors(TextColor.ANSI.YELLOW, TextColor.ANSI.BLACK);
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.ButtonField;
    }

    @Override
    public void draw(TextGraphics tg, TerminalPosition pos) {
        String text = getTitle() != null ? getTitle() : "";
        text += getLabel();
        tg.putString(pos, text);
    }

    public boolean haveOnClickEvent() {
        return onClick != null;
    }

    public void onButtonClick() {
        onClick.run();
    }

    @Override
    public void refreshCursorPosition(Terminal terminal, TerminalPosition pos) throws IOException {
        if (getTitle() != null) {
            pos = pos.withRelativeColumn(getTitle().length());
        }
        terminal.setCursorVisible(false);
        TextGraphics tg = terminal.newTextGraphics();
        tg.drawLine(pos.getColumn(), pos.getRow(), pos.getColumn() + getLabel().length() - 1, pos.getRow(), ' ');
        tg.setBackgroundColor(getHighlightBackgroundColor());
        tg.setForegroundColor(getHighlightForegroundColor());
        tg.putString(pos.getColumn(), pos.getRow(), getLabel());
        tg.setBackgroundColor(TextColor.ANSI.DEFAULT);
        tg.setForegroundColor(TextColor.ANSI.DEFAULT);
    }

    @Override
    public int getFieldWidth() {
        return getLabel().length() + (getTitle() != null ? getTitle().length() : 0);
    }

    public void setHighlightColors(TextColor.ANSI backgroundColor, TextColor.ANSI foregroudColor) {
        setHighlightBackgroundColor(backgroundColor);
        setHighlightForegroundColor(foregroudColor);
    }
}
