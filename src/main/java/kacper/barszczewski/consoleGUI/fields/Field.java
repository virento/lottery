package kacper.barszczewski.consoleGUI.fields;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.terminal.Terminal;

import java.io.IOException;

public class Field {

    protected boolean cursorEnable = false;

    public enum FieldType {
        Field, InputField, ButtonField, ListField;
    }

    private TextColor backgroundColor = TextColor.ANSI.DEFAULT;
    private TextColor foregroundColor = TextColor.ANSI.DEFAULT;

    private String label;

    public Field(String label) {
        this.label = label;
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean cursorEnable() {
        return cursorEnable;
    }

    public void setCursorEnable(boolean cursorEnable) {
        this.cursorEnable = cursorEnable;
    }

    public FieldType getFieldType() {
        return FieldType.Field;
    }

    public void onBackspace(Terminal terminal, TerminalPosition pos) throws IOException {

    }

    public void onCharacter(KeyStroke key, Terminal terminal, TerminalPosition pos) throws IOException {

    }

    public void draw(TextGraphics tg, TerminalPosition pos) {
        tg.putString(pos, getLabel());
    }

    public void refreshCursorPosition(Terminal terminal, TerminalPosition pos) throws IOException {

    }

    public void setColors(TextColor backgroundColor, TextColor foregroundColor) {
        this.backgroundColor = backgroundColor;
        this.foregroundColor = foregroundColor;
    }

    public TextColor getForegroundColor() {
        return foregroundColor;
    }

    public TextColor getBackgroundColor() {
        return backgroundColor;
    }

    public void onFocusLost() {

    }

    public void onFocusGain() {
    }

    public int getFieldWidth() {
        return getLabel().length();
    }
}
