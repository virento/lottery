package kacper.barszczewski.consoleGUI.fields;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.terminal.Terminal;
import kacper.barszczewski.entity.Value;

import java.io.IOException;
import java.util.ArrayList;

public class ListField<T extends Value> extends Field {

    private ArrayList<T> list = new ArrayList<>();

    private int selected = 0;
    private int firstShowing = 0;
    private int lastShowing = -1;

    private boolean focused = false;

    public ListField(String label) {
        super(label);
    }

    public ArrayList<T> getList() {
        return list;
    }

    public void addItem(T item) {
        list.add(item);
    }

    public void removeItem(T item) {
        int itemIndex = list.indexOf(item);
        if (selected != itemIndex) {
            selected++;
        } else if (selected == list.size() - 1) {
            selected--;
        }
        list.remove(item);
        if (list.size() == 0) {
            cursorEnable = false;
            selected = -1;
        }
    }

    @Override
    public boolean cursorEnable() {
        return list.size() != 0;
    }

    @Override
    public void refreshCursorPosition(Terminal terminal, TerminalPosition pos) throws IOException {
//        draw(terminal.newTextGraphics(), pos);
        int xPos = pos.getColumn();
        TextGraphics tg = terminal.newTextGraphics();
        for (int i = firstShowing; i < list.size(); i++) {
            T t = list.get(i);
            if (i == selected) {
                tg.setBackgroundColor(TextColor.ANSI.YELLOW);
                tg.putString(pos.withRelativeColumn(xPos), t.getValue());
                tg.setBackgroundColor(TextColor.ANSI.DEFAULT);
            } else {
                xPos += t.getValue().length() + 2;
            }
        }
    }

    @Override
    public void onFocusLost() {
        focused = false;
    }

    @Override
    public void onFocusGain() {
        focused = true;
    }

    @Override
    public void draw(TextGraphics tg, TerminalPosition pos) {
        if (list.size() == 0) return;
        int endScreen = tg.getSize().getColumns();

        for (int i = firstShowing; i < list.size(); i++) {
            T t = list.get(i);
            if (pos.getColumn() + t.getValue().length() + 2 > endScreen) {
                lastShowing = i - 1;
            } else {
                lastShowing = list.size() - 1;
            }
            if (focused && selected == i) {
                tg.setBackgroundColor(TextColor.ANSI.YELLOW);
                tg.putString(pos, t.getValue());
                tg.setBackgroundColor(TextColor.ANSI.DEFAULT);
            } else {
                tg.putString(pos, t.getValue());
            }
            pos = pos.withRelativeColumn(t.getValue().length());
            if (pos.getColumn() + 2 >= endScreen) {
                lastShowing = i - 1;
                break;
            }
            if (i != list.size() - 1) {
                tg.putString(pos, ", ");
                pos = pos.withRelativeColumn(2);
            }
        }
        tg.drawLine(pos.getColumn(), pos.getRow(), endScreen, pos.getRow(), ' ');
    }

    public void selectNext() {
        if (selected != list.size() - 1) {
            if (selected + 1 == lastShowing && lastShowing < list.size() - 1) {
                int newItemWidth = list.get(lastShowing).getValue().length() + 2;
                int width = 0;
                for (int i = selected; newItemWidth > width; i++) {
                    width += list.get(i).getValue().length() + 2;
                    firstShowing++;
                }
            }
            selected++;
        }
    }

    public void selectPrevious() {
        if (selected != 0) {
            if (selected - 1 == firstShowing && firstShowing > 0) {
                firstShowing--;
            }
            selected--;
        }
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.ListField;
    }

    public T getSelectedField() {
        if (selected != -1) {
            return list.get(selected);
        }
        return null;
    }

    public void setList(ArrayList<T> list) {
        this.list = list;
    }
}
