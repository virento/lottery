package kacper.barszczewski.consoleGUI.fields;

import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.input.KeyStroke;
import com.googlecode.lanterna.terminal.Terminal;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;

@Getter
@Setter
public class InputField extends Field {

    private int maxLength = 0;
    private String value = "";

    public InputField(String label) {
        super(label);
        cursorEnable = true;
    }

    public InputField(String label, int maxLength) {
        this(label);
        this.maxLength = maxLength;
    }

    @Override
    public boolean cursorEnable() {
        return cursorEnable;
    }

    @Override
    public FieldType getFieldType() {
        return FieldType.InputField;
    }

    @Override
    public void draw(TextGraphics tg, TerminalPosition pos) {
        tg.setBackgroundColor(getBackgroundColor());
        tg.setForegroundColor(getForegroundColor());
        tg.putString(pos, getLabel() + getValue());
    }

    @Override
    public void onCharacter(KeyStroke key, Terminal terminal, TerminalPosition pos) throws IOException {
        if (key.isCtrlDown() && (key.getCharacter() == 'n' || key.getCharacter() == 'N')) {
            return;
        }
        if (getValue().length() == getMaxLength()) return;
        TerminalPosition curPos = terminal.getCursorPosition();
        curPos = curPos.withRelativeColumn(-pos.getColumn());

        TextGraphics graphics = terminal.newTextGraphics();
        graphics.setBackgroundColor(getBackgroundColor());
        graphics.setForegroundColor(getForegroundColor());

        String value = getValue();
        int valuePos = curPos.getColumn() - getLabel().length();
        setValue(value.substring(0, valuePos) + key.getCharacter() + value.substring(valuePos));

        graphics.drawLine(pos.getColumn() + valuePos, pos.getRow(), pos.getColumn() + valuePos + 1, pos.getRow(), ' ');
        draw(graphics, pos);

        terminal.setCursorPosition(valuePos + getLabel().length() + 1 + pos.getColumn(), pos.getRow());
        terminal.flush();
    }

    @Override
    public void onBackspace(Terminal terminal, TerminalPosition pos) throws IOException {
        if (!cursorEnable()) return;
        TerminalPosition curPos = terminal.getCursorPosition();
        curPos = curPos.withRelativeColumn(-pos.getColumn());

        String value = getValue();
        if (getValue().length() == 0 || curPos.getColumn() == getLabel().length()) return;

        TextGraphics textGraphics = terminal.newTextGraphics();
        textGraphics.setBackgroundColor(getBackgroundColor());
        textGraphics.setForegroundColor(getForegroundColor());

        if (curPos.getColumn() == value.length() + getLabel().length()) {
            setValue(getValue().substring(0, getValue().length() - 1));
            textGraphics.putString(new TerminalPosition(curPos.getColumn() - 1 + pos.getColumn(), curPos.getRow()), " ");
            refreshCursorPosition(terminal, pos);
        } else {
            int columnPos = curPos.getColumn() - getLabel().length();
            setValue(value.substring(0, columnPos - 1) + value.substring(columnPos));
            int clearLength = getLabel().length() + getValue().length();
            textGraphics.drawLine(columnPos + pos.getColumn(), pos.getRow(), clearLength + pos.getColumn(), pos.getRow(), ' ');
            draw(textGraphics, pos);
            terminal.setCursorPosition(curPos.withRelativeColumn(pos.getColumn() - 1));
        }

        terminal.flush();
    }

    @Override
    public void refreshCursorPosition(Terminal terminal, TerminalPosition pos) throws IOException {
        terminal.setCursorVisible(true);
        terminal.setCursorPosition(getLabel().length() + getValue().length() + pos.getColumn(), pos.getRow());
    }

    @Override
    public int getFieldWidth() {
        return getLabel().length() + getValue().length();
    }
}
