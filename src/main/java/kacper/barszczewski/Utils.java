package kacper.barszczewski;


import com.googlecode.lanterna.TerminalPosition;
import com.googlecode.lanterna.TextCharacter;
import com.googlecode.lanterna.TextColor;
import com.googlecode.lanterna.graphics.BasicTextImage;
import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.graphics.TextImage;
import com.googlecode.lanterna.terminal.Terminal;
import kacper.barszczewski.entity.Lottery;
import kacper.barszczewski.entity.Participant;
import kacper.barszczewski.entity.Prize;
import kacper.barszczewski.consoleGUI.layers.Layer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Utils {

    public static void changeLayer(Terminal terminal, Layer oldLayer, Layer newLayer) throws IOException {
        if (oldLayer != null) {
            oldLayer.onLayerDetached(terminal);
        }
//        clearScreen(terminal);
        terminal.setBackgroundColor(TextColor.ANSI.BLACK);
        terminal.setForegroundColor(TextColor.ANSI.WHITE);
        terminal.clearScreen();
        newLayer.onLayoutAttached(terminal);
        newLayer.draw(terminal);
    }

    private static void clearScreen(Terminal terminal) throws IOException {
        TextGraphics tg = terminal.newTextGraphics();
        tg.setBackgroundColor(TextColor.ANSI.BLACK);
        tg.setForegroundColor(TextColor.ANSI.BLACK);
        TextImage image = new BasicTextImage(terminal.getTerminalSize());
        image.setAll(new TextCharacter(' ', TextColor.ANSI.BLACK, TextColor.ANSI.BLACK));
        tg.drawImage(new TerminalPosition(0,0), image);
    }

    public static boolean isEqual(char a, char b) {
        return Character.toLowerCase(a) == Character.toLowerCase(b);
    }


    public static ArrayList<Participant> participantsFromFile(File f) {
        try {
            return fromFile(Participant.class, f, String.class);
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public static ArrayList<Prize> prizesFromFile(File f) {
        try {
            return fromFile(Prize.class, f, String.class, String.class);
        } catch (IllegalAccessException | InvocationTargetException | InstantiationException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    public static <T> ArrayList<T> fromFile(Class<T> clazz, File f, Class<?>... parameters) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        if(f == null) return new ArrayList<>();
        Scanner sc;
        try {
            sc = new Scanner(f);
            sc.useDelimiter(";");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
        ArrayList<T> participants = new ArrayList<>();
        Constructor<T> constructor;
        try {
            constructor = clazz.getConstructor(parameters);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
        while (sc.hasNext()) {
            Object[] args = sc.next().split(",");
            participants.add(constructor.newInstance((Object[]) args));
        }
        sc.close();

        return participants;
    }

    public static boolean isEmpty(String lotteryDescription) {
        return lotteryDescription == null || lotteryDescription.length() == 0;
    }

    public static void exportToFile(Lottery lottery, File file) {
        if(file == null) {
            return;
        }
        try {
            if (!file.createNewFile()) {
                return;
            }
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        PrintWriter writer;
        try {
            writer = new PrintWriter(file);
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        writer.println("Title: " + lottery.getLotteryName());
        writer.println("Description: " + lottery.getLotteryDescription());
        writer.println("Winner - prize:");

        HashMap<Prize, Participant> winners = lottery.getWinners();
        for (Prize prize : winners.keySet()) {
            writer.println(prize.getName() + " - " + winners.get(prize).getName());
        }

        writer.close();
    }
}
