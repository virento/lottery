package kacper.barszczewski;

import kacper.barszczewski.entity.Lottery;

import java.util.List;

public interface ApplicationInterface {
    public void createLottery(Lottery lottery);

    public Lottery getLastLottery();

    int getLotterySize();

    public void closeApplication();

    public List<Lottery> getLotteries();

    public void switchToConsoleGui(GUI gui);

    public void switchToWindowGui(GUI gui);

    public Lottery getCreateLottery();

    public void setCreateLottery(Lottery lottery);
}
