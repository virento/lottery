package kacper.barszczewski.entity;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class ManyWinnersAlgorithm extends LotteryAlgorithm {

    @Override
    protected Participant chooseWinners(Prize prize) {
        return participants.get(Math.abs(getRand().nextInt()) % participants.size());
    }

    @Override
    public String toString() {
        return "Many prize for one person";
    }
}
