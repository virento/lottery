package kacper.barszczewski.entity;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public class OneWinnerAlgorithm extends LotteryAlgorithm {

    @Override
    protected Participant chooseWinners(Prize prize) {
        if(participants.size() == 0) return null;
        Participant participant = participants.get(Math.abs(getRand().nextInt()) % participants.size());
        participants.remove(participant);
        return participant;
    }

    @Override
    public String toString() {
        return "One prize for one person";
    }
}
