package kacper.barszczewski.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;


@Getter
@Setter
@NoArgsConstructor
public abstract class LotteryAlgorithm {

    protected ArrayList<Participant> participants;
    protected ArrayList<Prize> prizes;

    private HashMap<Prize, Participant> winners;

    private Random rand = new Random();

    public void roll() {
        if (participants == null) return;
        if (prizes == null) return;
        winners = new HashMap<>(prizes.size());
        for (Prize prize : prizes) {
            for (int i = 0; i < prize.getQuantity(); i++) {
                Participant winner = chooseWinners(prize);
                if (winner == null) {
                    return;
                }
                winners.put(prize.clone(), winner);
            }
        }
    }

    protected abstract Participant chooseWinners(Prize prize);
}
