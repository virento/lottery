package kacper.barszczewski.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Prize extends Value implements Cloneable {
    private int quantity;
    private String name;

    public Prize(int quantity, String name) {
        this.quantity = quantity;
        this.name = name;
    }

    public Prize(String name, String quantity) {
        try {
            this.quantity = Integer.parseInt(quantity);
        } catch (NumberFormatException ignore) {
            this.quantity = 0;
        }
        this.name = name;
    }

    @Override
    public String getValue() {
        String str = name;
        if (quantity > 1) {
            str += "(" + quantity + ")";
        }
        return str;
    }

    @Override
    protected Prize clone() {
        return new Prize(1, getName());
    }

    @Override
    public String toString() {
        return getValue();
    }
}
