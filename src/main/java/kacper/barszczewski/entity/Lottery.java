package kacper.barszczewski.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.HashMap;

@Getter
@Setter
@NoArgsConstructor
public class Lottery {
    private ArrayList<Participant> participants = new ArrayList<>();
    private ArrayList<Prize> prizes = new ArrayList<>();
    private LotteryAlgorithm algorithm = new OneWinnerAlgorithm();
    private String lotteryName = "";
    private String lotteryDescription = "";

    public void roll() {
        algorithm.setPrizes(prizes);
        algorithm.setParticipants(participants);
        algorithm.roll();
    }

    public HashMap<Prize, Participant> getWinners() {
        return algorithm.getWinners();
    }

    @Override
    public String toString() {
        return lotteryName;
    }
}
