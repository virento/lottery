package kacper.barszczewski.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class Participant extends Value {

    public String name;

    @Override
    public String getValue() {
        return name;
    }

    @Override
    public String toString() {
        return getValue();
    }
}
